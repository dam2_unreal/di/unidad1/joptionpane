package jOptionPane;

import javax.swing.JFrame;

public class Ventana extends JFrame{
	
	private PanelShowMessageDialog psmd;
	private PanelShowInputDialog psid;

	public Ventana(){
		super("JOptionPane");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(200, 50, 700, 600);
		
		psmd =  new PanelShowMessageDialog();
		psid =  new PanelShowInputDialog();
		
		this.add(psmd);
		this.add(psid);
	}
	
}