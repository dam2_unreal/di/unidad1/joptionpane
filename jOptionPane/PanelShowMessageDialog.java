package jOptionPane;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class PanelShowMessageDialog extends JPanel{
	

	public PanelShowMessageDialog(){	
		MiIcon ic = new MiIcon();
		
		JOptionPane.showMessageDialog(null, "Mensaje con 2 parametros");
		JOptionPane.showMessageDialog(null, "Mensaje con 3 argumentos", "F", JOptionPane.ERROR_MESSAGE);
		JOptionPane.showMessageDialog(null, "Mensaje con 5 argumentos", "Fx5", JOptionPane.PLAIN_MESSAGE, ic);
	}
	
	
}