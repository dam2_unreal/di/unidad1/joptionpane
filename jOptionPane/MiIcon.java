package jOptionPane;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class MiIcon implements Icon {

	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		
		Image img = new ImageIcon(getClass().getResource("../Recursos/Icon.png")).getImage();
		g.drawImage(img, 0, 0, null);
		
	}

	@Override
	public int getIconWidth() {
		return 50;
	}

	@Override
	public int getIconHeight() {
		return 50;
	}

}
