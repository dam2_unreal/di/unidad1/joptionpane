package jOptionPane;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class PanelShowInputDialog extends JPanel{

	private String resp;
	private String[] opciones = {"ACDAT","DI","Android","PSPRO","HLC","Sistemas","FOL Empresas"};
	private MiIcon ico;
	
	public PanelShowInputDialog() {
		
		//resp = JOptionPane.showInputDialog("Holi");
		//resp = JOptionPane.showInputDialog(null, "A", "B", JOptionPane.QUESTION_MESSAGE);
		resp = (String) JOptionPane.showInputDialog(null, "Elige el modulo", "Modulos", JOptionPane.INFORMATION_MESSAGE, ico, opciones, opciones[0]);
		
		tuRespuesta();		
	}
	
	public void tuRespuesta() {
		System.out.println("Tu respuesta es: "+resp);
	}

}
